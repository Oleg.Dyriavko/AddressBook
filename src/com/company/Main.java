package com.company;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        List<Human> humans = new ArrayList<>();

        humans.add(new Human("Ivan", "Ivanov", "380675556408"));
        humans.add(new Human("Sergey", "Petrov", "380506547895"));
        humans.add(new Human("Vadim", "Sidorov", "380667412589"));
        humans.add(new Human("Olga", "Danchenko", "380678885504"));
        humans.add(new Human("Yuliya", "Gordiyenko", "380509998877"));
        humans.add(new Human("Arthur", "Bliznyuk", "380633335564"));
        humans.add(new Human("Denis", "Adamenko", "380506996699"));
        humans.add(new Human("Albina", "Semenyuk", "380678881109"));
        humans.add(new Human("Sabrina", "Kravchenko", "380689998888"));
        humans.add(new Human("Miron", "Tokarev", "380675555555"));

        Book book = new Book(humans);

        humans.forEach(human -> System.out.println(" " + human.getFistName() + " " + human.getSecondName() + "  "
                + human.getPhoneNumber()));

        Scanner scanner = new Scanner(System.in);

        int choice = 0;
        while (choice > 0 || choice < 4) {
            System.out.println("\nMake your choice please:" +
                    "\n 1 = Sort by fistname;" +
                    "\n 2 = Sort by secondname;" +
                    "\n 3 = Filter by fistname and secondname;" +
                    "\n 4 = Filter by phone.");

            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    book.sortByFirstname();
                    break;
                case 2:
                    book.sortBySecondname();
                    break;
                case 3:
                    book.filterByFirstnameAndSecondname();
                    break;
                case 4:
                    book.filterByPhone();
                    break;
            }
        }
        System.out.println("You entered an incorrect value !");
    }
}


