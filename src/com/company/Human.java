package com.company;

public class Human {

    private String fistName;
    private String secondName;
    private String phoneNumber;

    public Human(String fistName, String secondName, String phoneNumber) {
        this.fistName = fistName;
        this.secondName = secondName;
        this.phoneNumber = phoneNumber;
    }

    public String getFistName() {
        return fistName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setFistName(String fistName) {
        this.fistName = fistName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}


