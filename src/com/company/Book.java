package com.company;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Book implements Comparable {

    private List<Human> humans;
    Scanner scanner = new Scanner(System.in);

    public Book(List<Human> humans) {
        this.humans = humans;
    }

    public List sortByFirstname() {

        List<String> strings =
                humans.stream()
                        .sorted((o1, o2) -> o1.getFistName().compareTo(o2.getFistName()))
                        .map(human -> human.getFistName() + " " + human.getSecondName() + " " + human.getPhoneNumber())
                        .collect(Collectors.toList());
        System.out.println(strings);
        return strings;
    }

    //((o1,o2) -> -o1.getName().compareTo(o2.getName()))

    public List sortBySecondname() {

       List<String> strings =
               humans.stream()
                //.sorted((o1, o2) -> o1.getSecondName().compareTo(o2.getSecondName()))
                .sorted((o1, o2) -> o1.getSecondName().compareTo(o2.getSecondName()))
                .map(human -> human.getFistName() + " " + human.getSecondName() + " " + human.getPhoneNumber())
                .collect(Collectors.toList());
        System.out.println(strings);
        return strings;
    }

    public List filterByFirstnameAndSecondname() {

        System.out.println("Enter the search firstname:");
        String f = scanner.nextLine();

        System.out.println("Enter the search secondname:");
        String s = scanner.nextLine();

        List<String> strings =
                humans.stream()
                        .filter(human -> human.getFistName().contains(f)&& human.getSecondName().contains(s))
                        .map(human -> human.getFistName() + " " + human.getSecondName() + " " + human.getPhoneNumber())
                        .collect(Collectors.toList());

        System.out.println(strings);

        return humans;
    }


    public List filterByPhone() {

        System.out.println("Enter the search Phone Number:");
        String p = scanner.nextLine();

        List<String> strings =
                humans.stream()
                        .filter(human -> human.getPhoneNumber().contains(p))
                        .map(human -> human.getFistName() + " " + human.getSecondName() + " " + human.getPhoneNumber())
                        .collect(Collectors.toList());

        System.out.println(strings);
        return humans;
    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }

}
